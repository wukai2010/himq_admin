module.exports = {
    publicPath: '',
    devServer: {
        port:7000,
        proxy: {  //配置跨域
            '/': {
                target: 'http://localhost:9999/',  //这里后台的地址模拟的;应该填写你们真实的后台接口
                changOrigin: true,  //允许跨域
            },
        },
    },
}