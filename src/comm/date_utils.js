export function getTimeSeg() {
    let now = new Date()
    let hour = now.getHours()
    let text = '';
    if (hour < 6) {
        text = '凌晨好'
    } else if (hour < 9) {
        text = '早上好'
    } else if (hour < 12) {
        text = '上午好'
    } else if (hour < 14) {
        text = '中午好'
    } else if (hour < 17) {
        text = '下午好'
    } else if (hour < 19) {
        text = '傍晚好'
    } else if (hour < 22) {
        text = '晚上好'
    } else {
        text = '夜里好'
    }
    return text;
}

export function rTime(date) {
    var json_date = new Date(date).toJSON();
    return new Date(new Date(json_date) + 8 * 3600 * 1000)
        .toISOString()
        .replace(/T/g, ' ')
        .replace(/\.[\d]{3}Z/, '')
}


// 格式化日期，如月、日、时、分、秒保证为2位数
function formatNumber (n) {
    n = n.toString()
    return n[1] ? n : '0' + n;
}
// 参数number为毫秒时间戳，format为需要转换成的日期格式
export function formatTime (number, format) {
    let time = new Date(number)
    let newArr = []
    let formatArr = ['Y', 'M', 'D', 'h', 'm', 's']
    newArr.push(time.getFullYear())
    newArr.push(formatNumber(time.getMonth() + 1))
    newArr.push(formatNumber(time.getDate()))

    newArr.push(formatNumber(time.getHours()))
    newArr.push(formatNumber(time.getMinutes()))
    newArr.push(formatNumber(time.getSeconds()))

    for (let i in newArr) {
        format = format.replace(formatArr[i], newArr[i])
    }
    return format;
}