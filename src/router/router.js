import VueRouter from "vue-router";
//import store from "../store";

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
import Vue from "vue";
import AdminLogin from "../page/AdminLogin";
import AdminPage from "../page/AdminPage";
import AppLoading from "../page/AppLoading";
import EditPassPage from "../page/EditPassPage";
import AdminIndex from "../page/AdminIndex";
import AdminSetting from "../page/AdminSetting";
import AdminAuth from "../page/AdminAuth";
import AdminSession from "../page/AdminSession";
import AdminApp from "../page/AdminApp";
import AdminDevice from "../page/AdminDevice";
import AdminSubscriber from "../page/AdminSubscriber";
import AdminConn from "../page/AdminLink";
import AdminUndelivered from "../page/AdminUndelivered";
import AdminRetain from "../page/AdminRetain";
import AdminPush from "../page/AdminPushList";
import AdminCommand from "../page/AdminCommand";
import AdminLog from "../page/AdminLog";
Vue.config.productionTip = false
Vue.use(VueRouter)

const  routes = {routes:[
        {path:'/login',name:'login',component:AdminLogin,meta:{title:"用户登录",closeEnable:true}},
        {path:'/admin',name:'admin',
            component:AdminPage,
            meta:{title:"Himq后台管理",closeEnable:false},
            children:[
                {path:'/admin/index',name:'admin_index',component:AdminIndex,meta:{title:"系统首页",closeEnable:false}},
                {path:'/admin/setting',name:'admin_setting',component:AdminSetting,meta:{title:"系统设置",closeEnable:true}},
                {path:'/admin/reset/password',name:'admin_edit_password',component:EditPassPage,meta:{title:"修改密码",closeEnable:true}},
                {path:'/admin/mqtt/user',name:'admin_mqtt_auth',component:AdminAuth,meta:{title:"授权管理",closeEnable:true}},
                {path:'/admin/session',name:'admin_session',component:AdminSession,meta:{title:"在线列表",closeEnable:true}},
                {path:'/admin/app',name:'admin_app',component:AdminApp,meta:{title:"应用管理",closeEnable:true}},
                {path:'/admin/device',name:'admin_device',component:AdminDevice,meta:{title:"设备管理",closeEnable:true}},
                {path:'/admin/subscriber',name:'admin_subscriber',component:AdminSubscriber,meta:{title:"订阅列表",closeEnable:true}},
                {path:'/admin/conn',name:'admin_conn',component:AdminConn,meta:{title:"连接日志",closeEnable:true}},
                {path:'/admin/push',name:'admin_push',component:AdminPush,meta:{title:"推送消息",closeEnable:true}},
                {path:'/admin/retain',name:'admin_retain',component:AdminRetain,meta:{title:"保留消息",closeEnable:true}},
                {path:'/admin/undelivered',name:'admin_undelivered',component:AdminUndelivered,meta:{title:"离线消息",closeEnable:true}},
                {path:'/admin/debug_log',name:'admin_debug_log',component:AdminLog,meta:{title:"实时日志",closeEnable:true}},
                {path:'/admin/debug_cmd',name:'admin_debug_cmd',component:AdminCommand,meta:{title:"指令列表",closeEnable:true}},
            ]
        },
        {path:'/loading',name:'loading',component:AppLoading,meta:{title:"正在加载中..",closeEnable:false}},
    ]}
const router = new VueRouter(routes);

router.beforeEach((to, from, next) => {

    /* 路由发生变化修改页面title */
    if (to.meta.title) {
        document.title = to.meta.title
    }
    next()
})

export default router;