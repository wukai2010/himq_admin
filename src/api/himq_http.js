//后台登录用户重置密码
import {post, get, del, post_header} from '../comm/http'

export function resetPass(data) {
    return post('/admin/api/reset/password',data);
}
export function login(data) {
    return post('/admin/api/login',data);
}
export function logout(data) {
    return post('/admin/api/logout',data);
}

///admin/api/mqtt/user

export function add_mqtt_user(data) {
    return post('/admin/api/mqtt/user',data);
}

export function del_mqtt_user(data) {
    return post('/admin/api/mqtt/user/delete',data);
}

export function get_mqtt_user(data) {
    return get('/admin/api/mqtt/user',data);
}

///admin/api/mqtt/session/list

export function get_session(data) {
    return get('/admin/api/mqtt/session/list',data);
}

///admin/api/mqtt/session/list

export function get_subscribe(data) {
    return get('/admin/api/subscribe',data);
}

export function get_link_log(data) {
    return get('/admin/api/mqtt/link_log',data);
}

export function get_retain(data) {
    return get('/admin/api/mqtt/retain',data);
}

export function get_undelivered(data) {
    return get('/admin/api/mqtt/undelivered',data);
}
///admin/api/mqtt/undelivered
///admin/api//sysinfo

export function sys_info(data) {
    return get('/admin/api/sysinfo',data);
}

export function get_app(data) {
    return get('/admin/api/app',data);
}

export function save_app(data) {
    return post('/admin/api/app',data);
}

export function delete_app(data) {
    return post('/admin/api/app/delete',data);
}


export function get_device(data) {
    return get('/admin/api/device',data);
}

export function save_device(data) {
    return post('/admin/api/device',data);
}

export function delete_device(data) {
    return post('/admin/api/device/delete',data);
}


///admin/api/command
export function get_command(data) {
    return get('/admin/api/command',data);
}

export function save_command(data) {
    return post('/admin/api/command',data);
}

export function delete_command(data) {
    return del('/admin/api/command',data);
}

///himq/api/push
export function push(data,appid) {
    return post_header('himq/api/push',data,{headers:{appid:appid}});
}

///himq/api/push
export function get_push(data) {
    return get('/admin/api/push/list',data);
}

export function del_push(data) {
    return post('/admin/api/push/delete',data);
}

///admin/api/setting/base

export function save_baseSetting(data) {
    return post('/admin/api/setting/base',data);
}
export function save_pushSetting(data) {
    return post('/admin/api/setting/push',data);
}

export function get_baseSetting(data) {
    return get('/admin/api/setting/base',data);
}
export function get_pushSetting(data) {
    return get('/admin/api/setting/push',data);
}

export function disconnect(data) {
    return post("/admin/api/mqtt/disconnect",data);
}