import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import router from "./router/router";
Vue.use(ElementUI);
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false
import store from './store/index'
import { Message as message } from 'element-ui';
Vue.prototype.$toast=message;
let app = new Vue({
  router,
  store,
  render: h => h(App)
});
window.vue = app;
app.$mount('#app')