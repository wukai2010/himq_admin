import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        panelTab: new Map(),
        //当前激活的tab选项卡
        tab_active: '',
    },
    mutations: {
        addTabs: function (state, menu) {
            let map = new Map();

            if (state.panelTab.get(menu.name)){
                return
            }

            state.panelTab.forEach(function (value, key) {
                map.set(key, value)
            })
            if (!state.panelTab.has(menu.name)) {
                map.set(menu.name, menu)
            }
            console.log('addTabMenu:', menu);
            state.panelTab = map;
        },
        removeTab: function (state, key) {
            let map = new Map();
            console.log('menu:', key);
            state.panelTab.delete(key)

            console.log('tab remove:', state.panelTab);
            state.panelTab.forEach(function (value, key) {
                map.set(key, value)
            })
            state.panelTab = map;
        },
        removeAllTab: function (state) {
            let map = new Map();

            console.log('tab remove:', state.panelTab);
            state.panelTab.forEach(function (value, key) {
                if (!value.enableClose){
                    map.set(key, value)
                }
            })
            state.panelTab = map;
        },

        removeOther: function (state) {
            let map = new Map();

            console.log('tab remove:', state.panelTab);
            state.panelTab.forEach(function (value, key) {
                //如果当前选项卡不可关闭
                if (!value.enableClose){
                    map.set(key, value)
                }
                //如果当前选项卡等于目前选中的选项卡
                if (key === state.tab_active){
                    map.set(key,value)
                }
            })
            state.panelTab = map;
        },
    },
    modules:
        {}
})
